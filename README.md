﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Tercera Práctica
## Resolución con paso de mensajes

Para la resolución de análisis y diseño se deberán utilizar paso de mensajes asíncronos como herramienta para la programación concurrente.

Para la implementación de la práctica se utilizará como herramienta de concurrencia JMS (Java Message Service). Esta práctica es una práctica en grupo de hasta dos alumnos y cada grupo deberá crear en el _broker_ sus propios _destinos_ para sus mensajes. Cada destino deberá definirse siguiendo la siguiente estructura:

```
....
// En la interface Constantes del proyecto 
public static final String DESTINO =
 "ssccdd.curso2021.NOMBRE_GRUPO.BUZON";
...
```

El nombre del grupo tiene que ser único para los grupos, por lo que se recomienda usar alguna combinación de los nombres de los integrantes del grupo.

## Problema a resolver
Para un restaurante tenemos dos tipos de clientes: clientes **estándar** y **premium**. Como los clientes premium tienen una tarjetan de fidelización, por la que pagan una cuota y solicitan platos más caros, el restaurante ha decidido dar prioridad a los clientes premium.

- Los clientes tienen priorizada la entrada al restaurante. Los clientes pueden llegar en un mismo momento al restaurante (cada cliente tarda en llegar un tiempo entre 0 y 10 segundos). Simulamos la generación de clientes de forma aleatoria y uniforme  (la mitad premium y la otra mitad estándar) y un espacio del restaurante para 10 personas. Entran mientras haya huecos, pero cuando se llena deben esperar en una cola. Cuando un cliente sale entra otro, pero siempre que haya un hueco libre entra un cliente premium primero, aunque el estándar lleve más rato esperando. Habrá que establecer algún criterio para que los clientes estándar no esperen de forma indefinida.

- Cuando los clientes están en el restaurante piden varios platos de diferente precio. Los clientes piden entre 3-5 platos. Los premium piden de un precio entre 10-40 € y los clientes estándar entre 5-20 €. Todos se sientan en mesas individuales y también piden y pagan individualmente.

- La cocina atiende los platos, no en orden de pedida, sino preparando el de precio más alto para que los clientes premium esperen menos. El tiempo que se tarda en preparar un plato estará simulado y será de un segundo.

- Cuando un plato de un cliente ha sido preparado, el cliente se lo puede comer. Para simplificar supondremos que el camarelo lo ha servido en su mesa. El cliente tarda entre 2-5 segundos en comerlo.

- Cuando acaba con el último plato, el cliente se marcha.

Para la solución hay que tener presente:

- Definir todas las constantes necesarias.

Para la implementación se tendrá que mostrar la siguiente información:

- El momento que un cliente llega al restaurante.
- El momento en el que el cliente entra en el restaurante.
- Los platos que ha pedido el cliente y el precio de los mismos.
- El momento en que abandona el restaurante.
- Recaudación del restaurante.

### Restricciones

Para la práctica cada integrante del grupo deberá repartirse el trabajo y para ello deberán definirse los siguientes procesos:

- ProcesoCliente, ProcesoPrincipal y los elementos comunes para el desarrollo del proyecto. En proceso cliente debe simular las acciones para el cliente con el restaurante. El proceso principal será el encargado para ir creando procesos cliente y obtener presentar la información que se pide a la finalización del restaurante.

- ProcesoCocinero y ProcesoRestaurante. El proceso cocinero será el encargado de preparar los diferentes platos que pide el cliente al restaurante. El proceso restaurante deberá centralizar las acciones que se llevarán acabo dentro del restaurante para atender a los clientes.
